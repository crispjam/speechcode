import { forwardRef } from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';

// https://mui.com/material-ui/react-snackbar/
export function SnackbarUI(props) {
  const { isOpen, text, isSuccess, handleClose } = props;

  const Alert = forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
  ) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });
  
  return (
    <Snackbar
      autoHideDuration={1250}
      open={isOpen}
      anchorOrigin={{ horizontal: 'center', vertical: 'top' }}
      onClose={handleClose}
    >
      <Alert onClose={handleClose} severity={isSuccess ? 'success' : 'error'} sx={{ width: '100%' }}>
        {text}
      </Alert>
    </Snackbar>
  )
}