import './recording-ui.css';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import MicIcon from '@mui/icons-material/Mic';
import StopIcon from '@mui/icons-material/Stop';
import UndoIcon from '@mui/icons-material/Undo';
import RedoIcon from '@mui/icons-material/Redo';
import CopyIcon from '@mui/icons-material/Assignment';

function RecordingUI(props) {
    const { isRecording, handleRecClick, handleUndo, handleRedo, handleCopy } = props;
    const startButton = (
      <Tooltip title="Start recognition">
        <IconButton
          color="primary"
          onClick={handleRecClick}
        >
          <MicIcon></MicIcon>
        </IconButton>
      </Tooltip>
    )

      const stopButton = (
        <Tooltip title="Stop recognition">
          <IconButton
            color="primary"
            onClick={handleRecClick}
          >
            <StopIcon />
          </IconButton>
        </Tooltip>
      )
    return (
      <div className="boxContainer">
        <div className={isRecording ? 'box box1' : 'box'}></div>
        <div className={isRecording ? 'box box2' : 'box'}></div>
        <div className={isRecording ? 'box box3' : 'box'}></div>
        <div className={isRecording ? 'box box4' : 'box'}></div>
        <div className={isRecording ? 'box box5' : 'box'}></div>
        {isRecording ? stopButton : startButton}
        <Tooltip title="Undo">
          <IconButton
            color="primary"
            onClick={handleUndo}
          >
            <UndoIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Redo">
          <IconButton
            color="primary"
            onClick={handleRedo}
          >
            <RedoIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Copy to clipboard">
          <IconButton
            color="primary"
            onClick={handleCopy}
          >
            <CopyIcon />
          </IconButton>
        </Tooltip>
      </div>
    )
}

export default RecordingUI;
