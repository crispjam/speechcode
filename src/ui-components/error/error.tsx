import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

export function Error() {
  return (
    <div>
    <h1>SPEECHCODE</h1>
    <Alert severity='error'>
      <AlertTitle><strong>Browser not compatible</strong></AlertTitle>
      Speechcode is only supported by Chrome on desktop and Android.
    </Alert>
    </div>
  )  
}
