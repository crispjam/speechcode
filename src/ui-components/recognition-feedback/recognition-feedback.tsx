import './recognition-feedback.css'

function RecognitionFeedback(props) {
  const { recognizedText, parseResult } = props;
  return (
    <div
      className={`recognized recognized-${parseResult}`}>
      {recognizedText}
    </div>
  )
}

export default RecognitionFeedback;
