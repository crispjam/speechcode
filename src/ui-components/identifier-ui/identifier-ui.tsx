import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';

type IdentifierUIProps = {
  handleIdentifierChange: any;
  currentId: string;
  idOptions: string[];
  parseState: 'regular' | 'newId' | 'existingId';
}

export function IdentifierUI(props: IdentifierUIProps) {
  const { currentId, handleIdentifierChange, idOptions, parseState } = props;
  const menuItems = idOptions.map(option => <MenuItem key={option} value={option}>{option}</MenuItem>);
  return (
    <div>
      {parseState === 'newId' ? (
        <TextField
          size="small"
          onChange={handleIdentifierChange}
          value={currentId}
        />
      ) : null}
      {parseState === 'existingId' ? (
        <Select
          size="small"
          value={currentId}
          onChange={handleIdentifierChange}
          defaultOpen={false}
        >
          {menuItems}
        </Select>
      ) : null}

    </div>
  )
}
