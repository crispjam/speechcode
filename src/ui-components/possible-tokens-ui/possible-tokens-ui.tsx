import { Item } from '../item/item';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';

type PossibleTokensUIProps = {
  tokenRegexes: [string, string][];
}

export function PossibleTokensUI(props: PossibleTokensUIProps) {
  const { tokenRegexes } = props;
  const items = tokenRegexes.map(pair => (
    <Item key={pair[0]}>{pair[0]}: {pair[1]}</Item>
  ));

  return (
    <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
      <Stack spacing={1}>
        <Item><b>Token options</b> <br /> (token: regex)</Item>
        <Divider></Divider>
        {items}
      </Stack>
    </Box>
  )
}