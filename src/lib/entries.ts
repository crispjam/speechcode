// Taken from:
// https://github.com/microsoft/TypeScript/issues/35101

type Entries<T> = {
  [K in keyof T]: [K, T[K]]
}[keyof T][]
  
export function entries<T>(obj: T): Entries<T> {
  return Object.entries(obj) as any;
}
