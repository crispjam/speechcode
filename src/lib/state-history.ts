export class StateHistory<StateType> {
  current: StateType;
  undoStack: StateType[];
  redoStack: StateType[];

  constructor(initial: StateType) {
    this.current = initial; 
    this.undoStack = [];
    this.redoStack = [];
  }

  add = (state: StateType): void => {
    this.redoStack = [];
    this.undoStack.push(this.current);
    this.current = state;
  }

  undo = (): StateType => {
    if (this.undoStack.length <= 0)
      return this.current;

    const state = this.undoStack.pop();
    this.redoStack.push(this.current);
    this.current = state;
    return this.current;
  }

  redo = (): StateType => {
    if (this.redoStack.length <= 0)
      return this.current;

    const state = this.redoStack.pop();
    this.undoStack.push(this.current);
    this.current = state;
    return this.current;
  }
}
