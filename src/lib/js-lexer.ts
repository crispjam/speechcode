import { JsToken } from "./js-token";
const Lexer = require("lex");

export class JsLexer {
  lexer: any;
  tokenRegexes: { [key: string]: string };
  constructor() {
    this.tokenRegexes = {};
    this.lexer = new Lexer();
    this.lexer.addRule(/\s/, () => {});
    this.lexer.addRule(/['"`,:!@#$%^&()~;{}[\]]/, () => {});

    this.addRule(/[0-9]+(\.[0-9]+)?/, (lexeme: string) => {
      return(new JsToken('tNumber', lexeme));
    });
    this.addRule(/(and)|(end)/, () => {
      return(new JsToken('tAnd', ' && '));
    });
    this.addRule(/or/, () => {
      return(new JsToken('tOr', ' || '));
    });
    this.addRule(/(is)?(greater|larger) (than)?/, () => {
      return(new JsToken('tGreater', ' > '));
    });
    this.addRule(/(is)?(less|smaller) (than)?/, () => {
      return(new JsToken('tLess', ' < '));
    });
    this.addRule(/(equal(s)?)|=/, () => {
      return(new JsToken('tEqual', ''));
    })
    this.addRule(/\+/, () => {
      return(new JsToken('tPlus', ' + '));
    });
    this.addRule(/-/, () => {
      return(new JsToken('tMinus', '-'));
    });
    this.addRule(/x|\*/, () => {
      return(new JsToken('tMultiply', ' * '));
    });
    this.addRule(/\//, () => {
      return(new JsToken('tDivide', ' / '));
    });
    this.addRule(/left bracket/, () => {
      return(new JsToken('tLeftBracket', '('));
    });
    this.addRule(/right bracket/, () => {
      return(new JsToken('tRightBracket', ')'));
    });
    this.addRule(/if/, () => {
      return(new JsToken('tIf', 'if'));
    });
    this.addRule(/else/, () => {
      return(new JsToken('tElse', 'else'));
    });
    this.addRule(/(then)|(than)/, () => {
      return(new JsToken('tThen', ''));
    });
    this.addRule(/finish block/, () => {
      return(new JsToken('tFinishBlock', ''))
    });
    this.addRule(/while/, () => {
      return(new JsToken('tWhile', 'while'))
    });
    this.addRule(/quote(s)?/, () => {
      return(new JsToken('tQuote', "'"))
    });
    this.addRule(/const(ant)?/, () => {
      return(new JsToken('tConst', 'const'))
    });
    this.addRule(/let/, () => {
      return(new JsToken('tLet', 'let'))
    });
    this.addRule(/assign(ed)?/, () => {
      return(new JsToken('tAssign', ''))
    });
    this.addRule(/list/, () => {
      return(new JsToken('tList', ''))
    });
    this.addRule(/finish list/, () => {
      return(new JsToken('tStopList', ''))
    });
    this.addRule(/next/, () => {
      return(new JsToken('tNext', ''))
    });
    this.addRule(/function/, () => {
      return(new JsToken('tFunction', 'function'))
    });
    this.addRule(/call/, () => {
      return(new JsToken('tCall', ''))
    });
    this.addRule(/return/, () => {
      return(new JsToken('tReturn', 'return'))
    });
    this.addRule(/nothing/, () => {
      return(new JsToken('tNothing', ''))
    });
    this.addRule(/for/, () => {
      return(new JsToken('tFor', 'for'))
    });
    this.addRule(/in/, () => {
      return(new JsToken('tIn', 'in'))
    });
    this.addRule(/of/, () => {
      return(new JsToken('tOf', 'of'))
    });
    this.addRule(/true/, () => {
      return(new JsToken('tTrue', 'true'))
    });
    this.addRule(/false/, () => {
      return(new JsToken('tFalse', 'false'))
    });
    this.addRule(/(log)|(console.log)/, () => {
      return(new JsToken('tConsoleLog', 'console.log'))
    });
    this.addRule(/undo/, () => {
      return(new JsToken('tUndo', ''))
    });
    this.addRule(/redo/, () => {
      return(new JsToken('tRedo', ''))
    });
    this.addRule(/stop/, () => {
      return(new JsToken('tStop', ''))
    });
    this.addRule(/[a-z]+/, (lexeme: string) => {
      return(new JsToken('tWord', lexeme))
    });
  }

  addRule = (pattern: RegExp, action: Function) => {
    const token: JsToken = action('');
    this.tokenRegexes[token.type] = pattern.toString();
    this.lexer.addRule(pattern, action);
  }

  scan = (input: string): JsToken[] => {
    this.lexer.setInput(input.toLowerCase());
    const result = [];
    while (this.lexer.index < input.length) {
      const token = this.lexer.lex()
      if (token !== undefined)
        result.push(token);
    }
    return result;
  }
}
