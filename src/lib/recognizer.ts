import { WebspeechApiWrapper } from './webspeech-api-wrapper';

export class Recognizer {
  toggleButtonElement: HTMLElement;
  recognitionCore: WebspeechApiWrapper;
  isRecognizing: boolean;
  
  constructor() {
    this.recognitionCore = new WebspeechApiWrapper();
    this.recognitionCore.onFinalAlternatives = (alternatives) => this.onFinalAlternatives(alternatives);
    this.recognitionCore.onInterimResult = (result) => this.onInterimResult(result);
  }

  onFinalAlternatives = (_alternatives: string[]): void => {};
  onInterimResult = (_result: string): void => {};

  toggleRecognition = (): void => {
    if (this.isRecognizing) {
      this.recognitionCore.stop()
      this.isRecognizing = false;
    } else {
      this.recognitionCore.start();
      this.isRecognizing = true;
    }
  }

}