import { entries } from './entries';

export abstract class Grammar {
  terminals: Set<string>;
  nonterminals: Set<string>;
  startSymbol: string;
  rules: { [key: string]: string[][]; };
  parseTable: { [key: string]: { [key: string]: string[] }};

  init = (): void => {
    this.terminals = new Set<string>();
    this.nonterminals = new Set<string>();
    const rules = entries(this.rules)

    this.startSymbol = rules[0][0];
    for (const [left, right] of rules) {
      this.nonterminals.add(left);
      for (const production of right)
        for (const symbol of production)
          if (symbol.startsWith('t'))
            this.terminals.add(symbol);
          else if (symbol.startsWith('n'))
            this.nonterminals.add(symbol);
    }

    this.parseTable = this.getParseTable();
  }

  isTerminal = (s: string): boolean => s.startsWith('t');
  isNonterminal = (s: string): boolean => s.startsWith('n');
  isPlaintext = (s: string): boolean => s.startsWith('p');

  getRulesWithBOnRight = (B: string) => {
    const rules = entries(this.rules);
    // return rules;
    return rules.map(rule => {
      const reducedRule: [string, any[]] = [rule[0], []];
      for (const production of rule[1])
        if (production.includes(B))
          reducedRule[1].push(this.filterPlaintext(production));
      return reducedRule;
    }).filter(rule => rule[1].length > 0);
  }

  filterPlaintext = (sequence: string[]): string[] =>
    sequence.filter((s) => !this.isPlaintext(s));

  getSetWithoutEpsilon = (s: Set<string>): Set<string> =>
    new Set([...s].filter(x => x !== 'tEpsilon'));

  getSetUnion = (a: Set<string>, b: Set<string>): Set<string> =>
    new Set([...a, ...b]);
  
  // Taken from: https://www.jambe.co.nz/UNI/FirstAndFollowSets.html
  // First 4
  getFirstOfSequence = (sequence: string[]): Set<string> => {
    // First 4.3
    if (sequence.length === 0)
      return new Set(['tEpsilon']);
    const firstOne = this.getFirstOfSymbol(sequence[0]);
    // First 4.1
    if (!firstOne.has('tEpsilon'))
      return firstOne;

    if (sequence.length > 1) {
      const firstRest = this.getFirstOfSequence(sequence.slice(1))
      // First 4.2
      return this.getSetUnion(this.getSetWithoutEpsilon(firstOne), firstRest);
    }
    return firstOne;
  }

  getFirstOfSymbol = (x: string): Set<string> => {
    // First 1
    if (this.isTerminal(x))
      return new Set([x]);

    if (this.isNonterminal(x)) {
      let firstX = new Set<string>();
      for (const production of this.rules[x]) {
        const filteredProduction = this.filterPlaintext(production);
        // First 2
        if (filteredProduction.includes('tEpsilon'))
          firstX.add('tEpsilon');
        // First 3
        else // TODO: maybe can remove
          firstX = this.getSetUnion(firstX, this.getFirstOfSequence(filteredProduction));
      }
      return firstX;
    }
  }

  getFollowOfNonterminal = (B: string, invokedBy: string = ''): Set<string> => {
    let followB = new Set<string>();
    const rules = this.getRulesWithBOnRight(B);
    for (const [left, right] of rules)
      for (const production of right) {
        const filteredProduction = this.filterPlaintext(production);
        const indexB = filteredProduction.indexOf(B);
        // Follow 2
        if (indexB === filteredProduction.length - 1) {
          if (left !== B && invokedBy !== left)
            followB = this.getSetUnion(followB, this.getFollowOfNonterminal(left, B));
        } else if (indexB !== -1) {
          const firstRest = this.getFirstOfSequence(filteredProduction.slice(indexB + 1));
          // Follow 4
          if (firstRest.has('tEpsilon')) {
            if (left !== B && invokedBy !== left)
              followB = this.getSetUnion(followB, this.getFollowOfNonterminal(left, B));
          }
          // Follow 3
          followB = this.getSetUnion(followB, this.getSetWithoutEpsilon(firstRest));
        }
      }
    return followB;
  }

  // Based on:
  // https://web.stanford.edu/class/archive/cs/cs143/cs143.1128/handouts/090%20Top-Down%20Parsing.pdf
  getParseTable = (): { [key: string]: { [key: string]: string[] }} => {
    const parseTable: { [key: string]: { [key: string]: string[] }} = {};
    for (const nonterminal of this.nonterminals) {
      parseTable[nonterminal] = { 'EOF': [] }; // TODO: remove EOF
      for (const terminal of this.terminals)
        parseTable[nonterminal][terminal] = [];
    }
    const rules = entries(this.rules)
    for (const [left, productions] of rules) {
      // console.log('left', left);
      for (const production of productions) {
        // console.log('getting first of production', production);
        const filteredProduction = this.filterPlaintext(production)
        const firstOfProduction: Set<string> = this.getFirstOfSequence(filteredProduction);
        // console.log('first of production:', firstOfProduction);
        for (const firstSymbol of firstOfProduction.values()) {
          // console.log('first symbol', firstSymbol);
          // Table 2
          if (this.isTerminal(firstSymbol)) {
            if (parseTable[left][firstSymbol].length > 0) {
              console.log(parseTable)
              console.log('nonterminal', left, 'terminal', firstSymbol);
              throw new Error('Grammar is not in LL(1) form, table construction failed');
            }
            parseTable[left][firstSymbol] = production;
          }
        }
        // Table 3
        if (firstOfProduction.has('tEpsilon')) {
          const followOfLeft = this.getFollowOfNonterminal(left);
          for (const followSymbol of followOfLeft) {
            if (this.isTerminal(followSymbol)) {
              if (parseTable[left][followSymbol].length > 0) {
                console.log(parseTable)
                console.log('nonterminal', left, 'terminal', followSymbol)
                throw new Error('Grammar is not in LL(1) form, table construction failed');
              }
              parseTable[left][followSymbol] = production;
            }
          }
        }
      }
    }

    return parseTable;
  }
}
