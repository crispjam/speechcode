import { Grammar } from "./grammar"
/*

TODO:
-----------
# Syntax rules
[x] While loops
[x] Identifiers
[x] Assignemnts
[x] Lists
[x] Functions
[x] For loops
[] Classes defitions
[] Object creation
[] Object method calling
[] console.log()
[] ternary operator assignment
[] switch statements

# General usability
[] Continuous dictation - part by part
[] Interim results of long dictation
[] Visual feedback and hints
[] Error correction

# Identifiers

## Advanced semantic checking (name resolution)
Built as a separate module

### Context building
- Holds a context - list of names in the given scope 
- When parser parses a new name assignment then name is added to context
- Somehow handle scope levels - exitting block should remove given scope level names from context

### Referencing
- Compare given name with context
- Try to find the most similar word (lavenshtein dist or other)
- If no name is similar enough, report a reference error

# Indentation
- Should work with context of semantic checker
- Enter block -> increase indentation
- Exit block -> decrease indentation

*/

export class JsGrammar extends Grammar {
  rules = {
    // Statements, kind off taken from (slide 3):
    // https://pages.cs.wisc.edu/~fischer/cs536.f13/lectures/f12/Lecture22.4up.pdf
    nStatements: [
      [ 'nStatement', 'p\n', 'nStatements'],
      [ 'tEpsilon' ],
    ],
    nStatement: [
      [ 'nIf' ],
      [ 'nWhile' ],
      [ 'nFor' ],
      [ 'nVariable' ],
      [ 'nFunctionDef' ],
      [ 'nFunctionCall' ],
      [ 'nConsoleLog' ],
      [ 'nReturn' ],
      // [ 'nExpr' ] Creates follow clash with identifier
    ],

    nBlock: [
      [ 'p {\n', 'nStatements', 'tFinishBlock', 'p}' ],
    ],

    // If statement
    nIf: [[ 'tIf', 'p (', 'nExpr', 'p)', 'nBlock', 'nMaybeElse' ]],
    nMaybeElse: [
      [ 'p ', 'tElse', 'nBlock' ],
      [ 'tEpsilon' ],
    ],


    // Loop statements
    nWhile: [[ 'tWhile', 'p (', 'nExpr', 'p)', 'nBlock' ]],

    nFor: [
      [ 'tFor', 'p (', 'nForHead', 'p)', 'nBlock' ],
    ],
    nForHead: [
      [ 'nClassicFor' ],
      [ 'nForOfIn' ],
    ],
    nClassicFor: [
      [ 'nVariable', 'tNext', 'p; ', 'nExpr', 'tNext', 'p; ', 'nExpr' ]
    ],
    nForOfIn: [
      ['nIdentifier', 'p ', 'nInOrOf', 'p ', 'nIdentifier' ]
    ],
    nInOrOf: [
      [ 'tIn' ],
      [ 'tOf' ],
    ],

    // Variables
    nVariable: [
      [ 'nNewVar' ],
      [ 'nReassign' ],
    ],
    nNewVar: [
      [ 'tConst', 'p ', 'nNewIdVar', 'nAssignRight' ],
      [ 'tLet', 'p ', 'nNewIdVar', 'nAssignRight' ],
    ],

    nReassign: [
      [ 'tAssign', 'nExistingIdVar', 'nAssignRight' ]
    ],

    nAssignRight: [
      [ 'tEqual', 'p = ', 'nExpr' ]
    ],

    nNewIdVar: [
      [ 'nIdentifier' ],
    ],

    nExistingIdVar: [
      [ 'nIdentifier' ],
    ],

    nNewIdFun: [
      [ 'nIdentifier' ],
    ],

    nExistingIdFun: [
      [ 'nIdentifier' ],
    ],

    // Identifier
    nIdentifier: [
      [ 'tWord', 'nIdWords' ]
    ],
    nIdWords: [
      [ 'tWord', 'nIdWords' ],
      [ 'tEpsilon']
    ],

    // Expressions taken from:
    // https://softwareengineering.stackexchange.com/a/364621
    nExpr: [[ 'nB', 'nE2' ]],
    nE2: [
      [ 'nLogOp', 'nB', 'nE2'],
      [ 'tEpsilon']
    ],
    nLogOp: [
      [ 'tAnd'],
      [ 'tOr' ]
    ],

    nB: [[ 'nA', 'nB2' ]],
    nB2: [
      [ 'nRelOp', 'nA', 'nB2'],
      [ 'tEpsilon' ]
    ],
    nRelOp: [
      [ 'tGreater' ],
      [ 'tLess' ],
      [ 'tEqual', 'p === '],
    ],

    nA: [[ 'nT', 'nA2' ]],
    nA2: [
      [ 'nAddOp', 'nT', 'nA2'],
      [ 'tEpsilon' ]
    ],
    nAddOp: [
      [ 'tPlus' ],
      [ 'p ', 'tMinus', 'p ' ]
    ],

    nT: [[ 'nF', 'nT2' ]],
    nT2: [
      [ 'nMulOp', 'nF', 'nT2'],
      [ 'tEpsilon' ]
    ],
    nMulOp: [
      [ 'tMultiply' ],
      [ 'tDivide' ]
    ],
    nF: [
      [ 'tLeftBracket', 'nExpr', 'tRightBracket' ],
      [ 'nStringLit' ],
      [ 'nIdentifier'],
      [ 'nListLit' ],
      [ 'nFunctionCall'],
      [ 'tNumber' ],
      [ 'tMinus', 'tNumber' ],
      [ 'tTrue' ],
      [ 'tFalse' ],
    ],

    // String literals
    // This construction might seems overly complicated
    // It is so that it enables spaces between words
    // without a dangling space at the end
    // and also an empty string
    nStringLit: [[ 'tQuote', 'nStrEnd']],
    nStrEnd: [
      [ 'tWord', 'nStrEnd2' ],
      [ 'tQuote' ]
    ],
    nStrEnd2: [
      [ 'p ', 'tWord', 'nStrEnd2' ],
      [ 'tQuote' ]
    ],

    nListLit: [[ 'p[', 'nList', 'p]' ]],

    // Lists
    nList: [[ 'tList', 'nListEnd' ]],
    nListEnd: [
      [ 'nExpr', 'nListEnd2' ],
      [ 'tStopList' ]
    ],
    nListEnd2: [
      [ 'tNext', 'p, ', 'nExpr', 'nListEnd2' ],
      [ 'tStopList' ]
    ],

    // Function definitions
    nFunctionDef: [
      [ 'tFunction', 'p ', 'nNewIdFun', 'p(', 'nDefArglist', 'p)', 'nBlock' ]
    ],
    nDefArglist: [[ 'tList', 'nDefArgListEnd' ]],
    nDefArgListEnd: [
      [ 'nIdentifier', 'nDefArgListEnd2'],
      [ 'tStopList' ],
    ],
    nDefArgListEnd2: [
      [ 'tNext', 'p, ', 'nIdentifier', 'nDefArgListEnd2' ],
      [ 'tStopList' ],
    ],

    // Function calls
    nFunctionCall: [[ 'tCall', 'nExistingIdFun', 'nArglist']],
    nConsoleLog: [[ 'tConsoleLog', 'nArglist' ]],
    nArglist: [[ 'p(', 'nList', 'p)']],

    // Return statement
    nReturn: [[ 'tReturn', 'nReturnValue']],
    nReturnValue: [
      [ 'p ', 'nExpr' ],
      [ 'tNothing' ],
    ]
  }
}
