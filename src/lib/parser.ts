import { Grammar } from "./grammar";
import { JsToken } from "./js-token";
import { StateHistory } from './state-history';
import { SemanticContext } from "./semantic-context";

export type ParseState = 'regular' | 'newId' | 'existingId';

export class Parser {
  grammar: Grammar;
  stack: string[];
  input: JsToken[];
  history: StateHistory<{ stack: string[], parseState: ParseState, currentId: string, idOptions: string[]}>;
  indentation: string;
  semanticContext: SemanticContext;
  parseState: ParseState;
  currentId: string;
  idOptions: string[];

  constructor(grammar: Grammar) {
    this.grammar = grammar;
    this.grammar.init();
    console.log('parse table', this.grammar.parseTable);
    this.stack = [ this.grammar.startSymbol ];
    this.semanticContext = new SemanticContext();
    this.indentation = '  ';
    this.parseState = 'regular';
    this.currentId = '';
    this.idOptions = [];
    this.history = new StateHistory<{ stack: string[], parseState: ParseState, currentId: string, idOptions: string[]}>({
      stack: this.stack,
      parseState: this.parseState,
      currentId: this.currentId,
      idOptions: this.idOptions,
    });
  }

  getPossibleTokens = (): string[] => {
    const possibleTokens: string[] = [ 'tUndo', 'tRedo', 'tStop'];
    for (const symbol of this.stack) {
      if (this.grammar.isTerminal(symbol)) {
        possibleTokens.push(symbol);
        break;
      }
      if (this.grammar.isPlaintext(symbol)) {
        continue;
      }
      let hasEpsilon = false;
      const symbolOptions: { [key: string]: string[] } = this.grammar.parseTable[symbol];
      for (const [token, production] of Object.entries(symbolOptions)) {
        if (production.length > 0) {
          if (production[0] === 'tEpsilon') {
            hasEpsilon = true;
          } else if (!possibleTokens.includes(token)) {
            possibleTokens.push(token);
          }
        }
      }
      if (!hasEpsilon)
        break;
    }
    return possibleTokens;
  }

  getIdOptions = (): string[] => {
    const closestIdTuples = this.semanticContext.getClosestIdTuples(this.currentId);
    if (closestIdTuples.length === 0)
      return [ this.currentId ];
    if (closestIdTuples[0][1] === 0)
      return closestIdTuples.map(idTuple => idTuple[0]);
    const idOptions = [ ...closestIdTuples.map(idTuple => idTuple[0]), this.currentId ]
    this.currentId = idOptions[0];
    return idOptions;
  }

  undo = () => {
    const { stack, currentId, parseState } = this.history.undo();
    this.stack = stack;
    this.currentId = currentId;
    this.parseState = parseState;
    this.semanticContext.undo();
  }

  redo = () => {
    const { stack, currentId, parseState } = this.history.redo();
    this.stack = stack;
    this.currentId = currentId;
    this.parseState = parseState;
    this.semanticContext.redo();
  }

  checkBlock = (stackItem: string) => {
    if (stackItem === 'nBlock')
      this.semanticContext.increaseBlockLevel();
    else if (stackItem === 'tFinishBlock')
      this.semanticContext.decreaseBlockLevel();
  }

  addIdWord = (word: string) => {
    if (this.currentId === '')
      this.currentId = word;
    else
      this.currentId += word[0].toUpperCase() + word.slice(1);
  }

  parseInput = (input: JsToken[]) => {
    // console.log('input', input);
    let textOutput = '';
    const stackCopy = [ ...this.stack ];
    // Process input
    while (input.length > 0) {
      this.checkBlock(stackCopy[0]);
      if (stackCopy[0] === input[0].type) {
        if (this.grammar.isTerminal(stackCopy[0])) {
          // console.log('matched', input[0]);
          stackCopy.shift();
          const token = input.shift();

          if (token.type === 'tWord') {
            if (this.parseState === 'regular') {
              textOutput += token.text;
            } else {
              this.addIdWord(token.text);
              if (this.parseState === 'existingId')
                this.idOptions = this.getIdOptions();
            }
          } else {
            if (this.parseState === 'newId') {
              this.semanticContext.addId(this.currentId);
              textOutput += this.currentId;
              this.currentId = '';
              this.parseState = 'regular';
            } else if (this.parseState === 'existingId') {
              textOutput += this.currentId;
              this.currentId = '';
              this.parseState = 'regular';
            }
            textOutput += token.text;
          }
        }
      } else if (this.grammar.isNonterminal(stackCopy[0])) {
        const X = stackCopy.shift();
        if (X === 'nStatement') {
          console.log('block level', this.semanticContext.blockLevel)
          textOutput += this.indentation.repeat(this.semanticContext.blockLevel); 
        }
        const prediction = this.grammar.parseTable[X][input[0].type];
        if (prediction === undefined) {
          throw new Error(`Parse ERROR, table cell is undefined because terminal ${input[0].type} is unused in grammar`);
        }
        if (prediction.length === 0) {
          throw new Error(`Parse ERROR, empty table cell M['${X}']['${input[0].type}']`);
        }
        if (X.startsWith('nNewId')) {
          this.parseState = 'newId'
          console.log('NEW ID');
        } else if (X.startsWith('nExistingId')) {
          this.parseState = 'existingId';
          console.log('EXISTING ID');
        }
        if (prediction[0] !== 'tEpsilon')
          stackCopy.unshift(...prediction);
      } else if (this.grammar.isPlaintext(stackCopy[0])) {
        const plaintext = stackCopy.shift();
        if (this.parseState === 'newId') { // Kind of a hack
          this.semanticContext.addId(this.currentId);
          textOutput += this.currentId;
          this.currentId = '';
          this.parseState = 'regular';
        } else if (this.parseState === 'existingId') {
          textOutput += this.currentId;
          this.currentId = '';
          this.parseState = 'regular';
        }
        if (plaintext === 'p}') { // HACKED: other indents are only for statements
          console.log('block level', this.semanticContext.blockLevel)
          textOutput += this.indentation.repeat(this.semanticContext.blockLevel); 
        }
        textOutput += plaintext.slice(1);
      } else {
        throw new Error(`Parse ERROR, mismatched nonterminal, expected ${stackCopy[0]}, got ${input[0].type}`)
      }
    }
    // Process all plaintext remaining in stack
    while (this.grammar.isPlaintext(stackCopy[0])) {
      const plaintext = stackCopy.shift();
      if (plaintext === 'p}') { // HACKED: other indents are only for statements
        console.log('block level', this.semanticContext.blockLevel)
        textOutput += this.indentation.repeat(this.semanticContext.blockLevel); 
      }
      textOutput += plaintext.slice(1);
    }

    // Whole input consumed without problems, returning text;
    this.stack = [ ...stackCopy ]
    
    // Only save stack into history, if text output is not empty
    // This way parser history stays in sync with editor history
    if (textOutput.length > 0)
      this.history.add({
        stack: this.stack,
        parseState: this.parseState,
        currentId: this.currentId,
        idOptions: this.idOptions,
      });
      this.semanticContext.saveHistory();

    return textOutput;
  }
}
