import levenshtein from 'js-levenshtein';
import { StateHistory } from './state-history';

export class SemanticContext {
  blockLevel: number;
  idObjects: { id: string, blockLevel: number }[]
  history: StateHistory<{ blockLevel: number, idObjects: { id: string, blockLevel: number }[] }>;

  constructor() {
    this.blockLevel = 0;
    this.idObjects = [];
    this.history = new StateHistory<{ blockLevel: number, idObjects: { id: string, blockLevel: number }[] }>({
      blockLevel: 0,
      idObjects: [],
    });
  }
  
  getClosestIdTuples = (currentId: string): [string, number][] => {
    const ids: [string, number][] = this.idObjects.map(idObject => [idObject.id, levenshtein(idObject.id, currentId)]);
    return ids.sort((a, b) => a[1] - b[1]);
  }

  increaseBlockLevel = (): void => {
    this.blockLevel += 1;
  }

  decreaseBlockLevel = (): void => {
    this.blockLevel -= 1;
    this.idObjects = this.idObjects.filter(idObject => idObject.blockLevel <= this.blockLevel);
  }

  addId = (id: string): void => {
    for (const idObject of this.idObjects)
      if (idObject.id === id)
        return
    this.idObjects.push({ id: id, blockLevel: this.blockLevel})
  }

  saveHistory = () => {
    this.history.add({
      blockLevel: this.blockLevel,
      idObjects: this.idObjects,
    });
  }

  undo = () => {
    const { blockLevel, idObjects } = this.history.undo();
    this.blockLevel = blockLevel;
    this.idObjects = idObjects; 
  }

  redo = () => {
    const { blockLevel, idObjects } = this.history.redo();
    this.blockLevel = blockLevel;
    this.idObjects = idObjects; 
  }
}
