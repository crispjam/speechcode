// https://stackoverflow.com/questions/38087013/angular2-web-speech-api-voice-recognition
const { webkitSpeechRecognition, webkitSpeechGrammarList } = (window as any);
const grammarWords = [
  'and', 'or', 'is', 'greater', 'than', 'larger', 'less', 'smaller',
  'equal', 'plus', 'minus', 'times', 'divided by', 'left bracket', 'right bracket',
  'if', 'else', 'finish block', 'while', 'quote', 'const', 'constant', 'let',
  'assign', 'list', 'finish list', 'next', 'function', 'call', 'return', 'nothing',
  'for', 'in', 'of', 'log', 'undo', 'redo', 'stop',
];

export class WebspeechApiWrapper {
  recognition: any;
  toggleButtonElement: HTMLElement;
  finalResultCount: number;
  
  onFinalAlternatives = (_alternatives: any): void => {};
  onInterimResult = (_alternatives: any): void => {};

  constructor() {
    this.recognition = new webkitSpeechRecognition();
    const speechRecognitionList = new webkitSpeechGrammarList();
    const grammar = `#JSGF V1.0; grammar keywords; public <keyword> = ${grammarWords.join(' | ')};`
    speechRecognitionList.addFromString(grammar, 1);

    this.recognition.grammars = speechRecognitionList;
    this.recognition.continuous = true;
    this.recognition.lang = 'en-US';
    this.recognition.interimResults = true;
    this.recognition.maxAlternatives = 10;
    this.recognition.onresult = this.handleResult;
    this.finalResultCount = 0;
  }
  start = (): void => {
    this.recognition.start();
    this.finalResultCount = 0;
  }
  stop = (): void => { this.recognition.stop(); }

  handleResult = (event) => {
    const currentResult = event.results[event.results.length - 1];
    if (currentResult.isFinal) {
      const currentAlternatives = [];
      for (let i = 0; i < currentResult.length; i++)
        currentAlternatives.push(currentResult[i].transcript.trim());
      this.onFinalAlternatives(currentAlternatives);
      this.finalResultCount = event.results.length;
    } else {
      let interimResult = '';
      for (let i = this.finalResultCount; i < event.results.length; i++)
        interimResult += event.results[i][0].transcript;
      this.onInterimResult(interimResult);
    }
  }
}
