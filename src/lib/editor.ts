import { EditorView } from "@codemirror/view";
import { EditorState } from "@codemirror/state";
import { autocompletion } from "@codemirror/autocomplete";
import { javascript } from "@codemirror/lang-javascript";
import { lineNumbers } from '@codemirror/gutter';
import { history, undo, redo } from '@codemirror/history';
import { oneDark } from '@codemirror/theme-one-dark';

// This is how to get text from codemirror view:
// https://discuss.codemirror.net/t/codemirror-6-proper-way-to-listen-for-changes/2395/4
export class Editor {
  view: EditorView;

  constructor(domElement) {
    this.view = new EditorView({
      state: EditorState.create({
        extensions: [
          autocompletion({
            activateOnTyping: true,
          }),
          javascript(),
          lineNumbers(),
          history(),
          oneDark,
          EditorView.editable.of(false),
          ]
      }),
      parent: domElement
    });
  }

  getText = (): string => {
    const lines = this.view.state.doc;
    let text = '';
    for (const line of lines)
      text += line;
    return text;
  }

  insertText = (text) => {
    this.view.dispatch({
      changes: { from: this.view.state.doc.length, insert: text }
    })
  }

  undoChange = () => {
    undo(this.view);
  }

  redoChange = () => {
    redo(this.view);
  }
}
