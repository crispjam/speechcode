import { Editor } from './lib/editor';
import { Recognizer } from './lib/recognizer';
import { Parser } from './lib/parser';
import { JsGrammar } from './lib/js-grammar';
import { JsLexer } from './lib/js-lexer';

import './App.css';
import { Component, createRef, RefObject } from 'react';
import RecordingUI from './ui-components/recording-ui/recording-ui';
import RecognitionFeedback from './ui-components/recognition-feedback/recognition-feedback';
import { IdentifierUI } from './ui-components/identifier-ui/identifier-ui';
import { PossibleTokensUI } from './ui-components/possible-tokens-ui/possible-tokens-ui';
import { Error } from './ui-components/error/error';
import { SnackbarUI } from './ui-components/snackbar-ui/snackbar-ui';

type AppState = {
  isError: boolean;
  isRecording: boolean;
  recognizedText: string;
  parseResult: 'success' | 'error' | 'pending';
  stack: string[];
  parseState: 'regular' | 'newId' | 'existingId';
  currentId: string;
  idOptions: string[];
  possibleTokenRegexes: [string, string][];
  isSnackbarOpen: boolean;
  isSnackSuccess: boolean;
  snackbarText: string;
}

class App extends Component<AppState> {
  state: AppState;
  editorDivRef: RefObject<HTMLDivElement>;
  textAreaRef: RefObject<HTMLTextAreaElement>
  editor: Editor;
  recognizer: Recognizer;
  parser: Parser;
  jsLexer: JsLexer;

  constructor(props) {
    super(props);
    this.state = {
      isError: false,
      isRecording: false,
      recognizedText: '',
      parseResult: 'pending',
      stack: [],
      parseState: 'regular',
      currentId: '',
      idOptions: [],
      possibleTokenRegexes: [],
      isSnackbarOpen: false,
      isSnackSuccess: true,
      snackbarText: '',
    }

    this.editorDivRef = createRef();
    this.textAreaRef = createRef();
    this.parser = new Parser(new JsGrammar());
    this.jsLexer = new JsLexer();
    
  }
  
  componentDidMount() {
    try {
      this.recognizer = new Recognizer();
      this.recognizer.onFinalAlternatives = this.handleFinalAlternatives;
      this.recognizer.onInterimResult = this.handleInterimResult;
      this.setState({ stack: this.parser.stack });
    } catch {
      this.setState({ isError: true });
    }
    this.editor = new Editor(this.editorDivRef.current);
    this.setTokenOptions();
  }

  handleRecClick = () => {
    this.recognizer.toggleRecognition();
    this.setState((prevState: AppState) => {
      return { isRecording: !prevState.isRecording };
    })
  }

  setTokenOptions = () => {
    const possibleTokens: string[] = this.parser.getPossibleTokens();
    const possibleTokenRegexes: [string, string][] = possibleTokens.map(token => [token, this.jsLexer.tokenRegexes[token]]);
    this.setState({ possibleTokenRegexes })
  }

  undo = () => {
    console.log('undoing');
    this.parser.undo();
    this.editor.undoChange();
    this.setTokenOptions();
  }

  redo = () => {
    console.log('redoing');
    this.parser.redo();
    this.editor.redoChange();
    this.setTokenOptions();
  }

  handleInterimResult = (result: string) => {
    this.setState({ recognizedText: result, parseResult: 'pending' });
  }

  handleCopy = () => {
    const text = this.editor.getText();
    window.navigator.clipboard.writeText(text).then(() => {
      this.setState({
        isSnackbarOpen: true,
        snackbarText: 'Copied to clipboard',
        isSnackSuccess: true,
      });
    }, () => {
      this.setState({
        isSnackBarOpen: true,
        snackbarText: 'Copy failed',
        isSnackSuccess: false,
      });
    })
  }

  handleFinalAlternatives = (alternatives: string[]) => {
    let parseResult = 'success';
    let recognizedText = alternatives[0];

    for (const alternative of alternatives) {
      parseResult = 'success';
      recognizedText = alternative;
      console.log('trying alternative:', alternative);
      const tokens = this.jsLexer.scan(alternative);
      
      if (tokens[0].type === 'tUndo') {
        this.undo();
        break;
      } else if (tokens[0].type === 'tRedo') {
        this.redo();
        break;
      } else if (tokens[0].type === 'tStop') {
        this.handleRecClick();
        break;
      } else {
        try {
          if (this.state.parseState !== 'regular') {
            this.parser.currentId = this.state.currentId;
          }
          const text = this.parser.parseInput(tokens);
          this.editor.insertText(text);
          break; // Alternative was successful, no need to try other ones
        } catch (e) {
          parseResult = 'error';
          recognizedText = alternatives[0];
          console.log(e);
        }
      }
    }

    this.setState({
      stack: this.parser.stack,
      parseResult: parseResult,
      recognizedText: recognizedText,
      parseState: this.parser.parseState,
      currentId: this.parser.currentId,
      idOptions: this.parser.idOptions,
    });
    this.setTokenOptions();
  }

  handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      this.handleFinalAlternatives([this.textAreaRef.current.value])
      this.textAreaRef.current.value = '';
    }
  }

  render() {
    const {
      isError, isRecording, recognizedText, parseResult,
      currentId, idOptions, parseState, possibleTokenRegexes,
      isSnackbarOpen, snackbarText, isSnackSuccess,
    } = this.state;
    if (isError)
      return(<Error />)

    return(
      <div className="app">
        <h1>SPEECHCODE</h1>
        <SnackbarUI
          isOpen={isSnackbarOpen}
          text={snackbarText}
          isSuccess={isSnackSuccess}
          handleClose={() => { this.setState({ isSnackbarOpen: false, snackbarText: '' })}}
        />
        <RecognitionFeedback
          recognizedText={recognizedText}
          parseResult={parseResult}
        />
        <RecordingUI
          isRecording={isRecording}
          handleRecClick={this.handleRecClick}
          handleUndo={this.undo}
          handleRedo={this.redo}
          handleCopy={this.handleCopy}
        />
        <IdentifierUI
          currentId={currentId}
          handleIdentifierChange={(event) => this.setState({ currentId: event.target.value })}
          idOptions={idOptions}
          parseState={parseState}
        />
        <div id='editorDivId' ref={this.editorDivRef}></div>
        <PossibleTokensUI
          tokenRegexes={possibleTokenRegexes}
        />
      </div>
    )
  }
}

export default App;
